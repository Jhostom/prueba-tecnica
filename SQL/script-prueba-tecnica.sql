CREATE DATABASE sociedades;
USE sociedades;

CREATE TABLE asociaciones(
	Id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Nombre VARCHAR(300) NOT NULL,
    NIT VARCHAR(300) UNIQUE NOT NULL,
    Direcciones VARCHAR(200) NOT NULL,
    Telefonos VARCHAR(100) NOT NULL,
    Tipo VARCHAR(300) NOT NULL
);

CREATE TABLE proyectos(
	Id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Objetivo VARCHAR(300) NOT NULL,
    Pais VARCHAR(300) UNIQUE NOT NULL,
    IdProyecto INT(11) NULL,
    FOREIGN KEY (IdProyecto) REFERENCES Proyectos(Id)
);

CREATE TABLE asociaciones_proyectos(
	Id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	IdAsociacion INT(11) NOT NULL,
    IdProyecto INT(11) NOT NULL,
    FOREIGN KEY (IdAsociacion) REFERENCES Asociaciones(Id),
    FOREIGN KEY (IdProyecto) REFERENCES Proyectos(Id)
);

CREATE TABLE grupos(
	Id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Nombre VARCHAR(300) NOT NULL
);

CREATE TABLE trabajadores(
	Id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Nombre VARCHAR(300) NOT NULL,
    Cedula VARCHAR(300) UNIQUE NOT NULL,
    Direccion VARCHAR(200) NOT NULL,
    Telefonos VARCHAR(100) NOT NULL,
    Genero VARCHAR(100) NOT NULL,
    Usuario VARCHAR(100) NOT NULL,
    Salario FLOAT NULL,
    Edad INT(11) NULL,
    Profesion VARCHAR(300) NULL,
    HorasAportadas INT NULL,
	FechaIngreso DATE NOT NULL,
    IdGrupo INT(11) NULL,
    FOREIGN KEY (IdGrupo) REFERENCES Grupos(Id)
);

CREATE TABLE socios(
	Id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    IdTrabajador INT(11) NOT NULL,
    FOREIGN KEY (IdTrabajador) REFERENCES Trabajadores(Id)
);

CREATE TABLE asociaciones_trabajadores(
	IdAsociacion INT(11) NOT NULL,
    IdTrabajador INT(11) NOT NULL UNIQUE,
    FOREIGN KEY (IdAsociacion) REFERENCES Asociaciones(Id),
    FOREIGN KEY (IdTrabajador) REFERENCES Trabajadores(Id)
);

CREATE TABLE asociaciones_socios(
	IdAsociacion INT(11) NOT NULL,
    IdSocio INT(11) NOT NULL,
    AporteCapital INT(11) NOT NULL    ,
    FOREIGN KEY (IdAsociacion) REFERENCES Asociaciones(Id),
    FOREIGN KEY (IdSocio) REFERENCES Socios(Id)
);

/* CONSULTAS */
-- A)
SELECT T.*, AT.IdAsociacion AS Asociacion FROM trabajadores T JOIN asociaciones_trabajadores AT ON AT.IdTrabajador = T.Id;
SELECT P.*, AP.IdAsociacion AS Asociacion FROM proyectos P JOIN asociaciones_proyectos AP ON AP.IdProyecto = P.Id;

-- B)
SELECT SUM(T.Edad) AS EdadTotal FROM trabajadores T WHERE T.Edad is not null GROUP BY T.IdGrupo;

-- C)
SELECT COUNT(T.Id) AS CantidadTrabajadores, T.* FROM trabajadores T WHERE T.Usuario like 'a%' GROUP BY T.Genero;