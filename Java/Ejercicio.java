package Java;
import java.util.Scanner;

public class Ejercicio {
    public static void main(String[] args) {

        int entrada = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Por favor ingrese un número:");
        entrada = Integer.parseInt(scanner.nextLine());
        System.out.println("---------------------------------------");

        for (int i = entrada; i < entrada + 10; i++) {

            System.out.println(fibonacciRecursivo(i));
        }
    }
    
    public static int fibonacciRecursivo(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return fibonacciRecursivo(n - 1) + fibonacciRecursivo(n - 2);
        }
    }
}