import random

listaNumeros = []

for number in range(0, 10):
    listaNumeros.append(random.randint(0, 100))

listaOrdenada = sorted(listaNumeros)
print(listaOrdenada)

numeroBuscar = int(input("Introduce el número a buscar: "))

bajo = 0
alto = len(listaOrdenada) - 1

def busqueda_recursiva(lista, bajo, alto, indice):
    if bajo > alto:
        return -1
    centro = (bajo + alto) // 2
    if lista[centro] == indice:
        return centro
    elif lista[centro] < indice:
        return busqueda_recursiva(lista, centro + 1, alto, indice)
    else:
        return busqueda_recursiva(lista, bajo, centro - 1, indice)

resultado = busqueda_recursiva(listaOrdenada, bajo, alto, numeroBuscar)

if resultado >= 0:
    print("El número fue encontrado.")
elif resultado == -1:
    print("El número no fue encontrado.")
